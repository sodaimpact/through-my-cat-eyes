extends Sprite

var imagetexture = ImageTexture.new()
var textr = self.get_texture()
var img2 = textr.get_data()
var img = Image.new() 
var decalage = 3 #définie l'amplitude de la déformaiton en pixel.
var dec
var att = 0 #définie le nombre de frame avant que déformation commence (à partir du chargement de la scène)
var spd = 4 #modifie la vitesse de la déformation, en frame d'attentes entre chaque update
var i = 0
var x
var y
var compt = 0



func _process(delta):
	compt = compt + 1 
	att = att + 1
	if compt > 2 :
		img.create(180,30,false,Image.FORMAT_RGBA8)
		img2.lock()
		img.lock()
		if att > spd :
			i = (i + 1)%(2 * decalage)
			att = 0
		for y in range (0,20) :
			if abs(y-i)/decalage % 2 == 0 :
				dec = abs(y-i)%decalage
			else :
				dec = decalage - abs(y-i)%decalage
			for x in range (8,159) :
				img.set_pixel(x-dec,y,img2.get_pixel(x,y))
		imagetexture.create_from_image(img)
		set_texture(imagetexture)
		update()
		img2.unlock()
		img.unlock()
		
	
	