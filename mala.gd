extends KinematicBody2D

var motion = Vector2()
const UP = Vector2(0,-1)
export var gravity = 10
export var speed = 43
var state = ["d","pyj","sac"]
var i = 0

func _physics_process(delta):
	
	motion.y += gravity
	
	if Input.is_action_pressed("ui_mala_d"):
		i = 0
	elif Input.is_action_pressed("ui_mala_pyj"):
		i = 1
	elif Input.is_action_pressed("ui_mala_sac"):
		i = 2
	
	#droite
	if Input.is_action_pressed("ui_mala_right"):
		$mala.flip_h = false
		$mala.play("marche_"+state[i])
		motion.x = speed
		
	#gauche
	elif Input.is_action_pressed("ui_mala_left"):
		$mala.flip_h = true
		$mala.play("marche_"+state[i])
		motion.x = -speed
	
	#arret
	else:
			motion.x = 0
			$mala.play("idle_"+state[i])
	
	motion = move_and_slide(motion, UP)
	
	pass
