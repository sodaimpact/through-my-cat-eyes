extends Area2D

var hit = divers.hit

func _ready():

	pass

func _process(delta):
	
	if hit == 0 :
		$arbre_avant.set_visible(true)
		$arbre_apres.set_visible(false)
		$arbre_hit.set_visible(false)
		var areas = get_overlapping_areas()
		for area in areas:
			if area.name == "AcoupR" or area.name == "AcoupL":
				$arbre_avant.set_visible(false)
				$arbre_apres.set_visible(false)
				$arbre_hit.set_visible(true)
				$arbre_hit/AP_arbre.queue("hit")
				divers.hit = 1
				hit = 1
	elif hit == 1 and not($arbre_hit/AP_arbre.is_playing()):
		$arbre_avant.set_visible(false)
		$arbre_apres.set_visible(true)
		$arbre_hit.set_visible(false)
		
		
	pass
