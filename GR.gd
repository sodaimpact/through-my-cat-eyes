extends Sprite

var finalmap = ImageTexture.new()
var sun = Vector2(-1,-1) #Position de la source de lumière
var b = 1    #Change la composante sur x du vecteur de déplacement vers la source de lumière.
var n = 0    #Nombre d'itérations à partir duquel seq n'est plus un pixel qui occlude.
var c = 0.05 #Change le coeficient par lequel la transparance décroît.
var d = 1    #change la valeur de départ de la transparance. 
var a = 0    #Pente de la droite pixel-source de lumière.
var seq = Vector2(0,0) 
var compt2 = 0

func _process(delta):
	compt2 = compt2+1
	if compt2 ==500 :
		var NonOcc = get_node("/root/GRtry/OccludingMap").bleu #Couleur pour laquelle un pixel de l'OccMap n'occlude pas.
		var OcMap = get_node("/root/GRtry/OccludingMap").imagetexture.get_data().lock()

		for y in range (0,75) :
			for x in range(0,160):
				a = abs(y-sun.y)/abs(x-sun.x)
				seq = Vector2(x,y)
				n = 0
				while OcMap.get_pixel(seq) != NonOcc :
					seq.x = seq.x + b
					seq.y = seq.y + b*a
					n=n+1
				var newa = max(d-n*c,0)
				finalmap.get_data().lock().set_pixel(NonOcc)
				finalmap.get_data().lock().get_pixel(x,y).a = newa
		set_texture(finalmap)
## Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
