extends Node2D

var noeud
var trackn
var trackec
var tracknec

func jouer(track,simple=0):
	trackn = self.conversion(track)
	trackec = self.encours()
	if simple != 0 :
		trackn.play()
			
	elif trackec == 0 :
		trackn.set_volume_db(-60)
		trackn.play()
		$Tween1.interpolate_property(trackn, "volume_db", -60, 0 , 3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween1.start()
	elif track != trackec :
		tracknec = self.conversion(trackec)
		trackn.set_volume_db(-60)
		trackn.play()
		$Tween1.interpolate_property(trackn, "volume_db", -60, 0 , 2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween2.interpolate_property(tracknec, "volume_db", 0, -60 , 2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween1.start()
		$Tween2.start()
	pass

func encours ():
	if $sakvill.is_playing() == true :
		return 9
	elif $foret.is_playing() == true :
		return 3
	elif $lac.is_playing() == true :
		return 2
	elif $intro.is_playing() == true :
		return 11
	elif $room.is_playing() == true :
		return 12
	elif $citywave.is_playing() == true :
		return 13
	elif $sakvill2.is_playing() == true :
		return 15
	else :
		return 0
		
func conversion (track) :
	if track == 9 :
		noeud = get_node("sakvill")
	elif track == 3 :
		noeud = get_node("foret")
	elif track == 2 : 
		noeud = get_node("lac")
	elif track == 11 :
		noeud = get_node("intro")
	elif track == 12 :
		noeud = get_node("room")
	elif track == 13 :
		noeud = get_node("citywave")
	elif track == 15 :
		noeud = get_node("sakvill2")
	return noeud
	

func _on_Tween2_tween_completed(object, key):
	tracknec.stop()
	pass

