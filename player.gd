extends KinematicBody2D

var motion = Vector2()
const UP = Vector2(0,-1)
export var gravity = 10
export var speed = 50 #50
var q = 0
var s = 0
var last

func _ready():
	$chat.play("idle")
	$chat_saut.set_frame(0)
	pass
	
func _physics_process(delta):
	
	motion.y += gravity
	
	if is_on_floor():  #empecher lors du saut de se deplacer
		#pas joli parce que la flemme de refaire tous les sprite a la bonne taille juste pour le saut pour l'insant
		if s == 0:
			$chat.show()
			$shape1.show()
			$chat_saut.hide()
			$shape2.hide()
			$chat_saut.set_frame(0)
		
		#droite
		if Input.is_action_just_pressed("ui_right"):
			q = 0
			s = 0
			$chat.flip_h = false
			$chat.play("transition1")
			motion.x = 0   #empecher la glissade
		elif Input.is_action_pressed("ui_right") and $chat.get_frame() == 1:
			$chat.flip_h = false
			$chat.play("marche")
			motion.x = speed
			last = "right"
		elif Input.is_action_just_released("ui_right"):
			$chat.flip_h = false
			$chat.play("transition2")
			motion.x = 0
			
		#gauche
		elif Input.is_action_just_pressed("ui_left"):
			q = 0
			s = 0
			$chat.flip_h = true
			$chat.play("transition1")
			motion.x = 0
		elif Input.is_action_pressed("ui_left") and $chat.get_frame() == 1:
			$chat.flip_h = true
			$chat.play("marche")
			motion.x = -speed
			last = "left"
		elif Input.is_action_just_released("ui_left"):
			$chat.flip_h = true
			$chat.play("transition2")
			motion.x = 0
		
		#saut
		elif Input.is_action_just_pressed("ui_up") and q == 0:
			if last == "right":
				$chat_saut.flip_h = false
			if last == "left":
				$chat_saut.flip_h = true
			s = 1
			$chat.hide()
			$shape1.hide()
			$chat_saut.show()
			$shape2.show()
			$chat_saut.play("pre_saut")
			motion.x = 0
		elif Input.is_action_pressed("ui_up") and $chat_saut.get_frame() == 1 and q == 0:
			$chat_saut.play("saut")
			motion.y = -170
			motion.x = 0
		elif $chat_saut.get_frame() == 2 :
			s = 0
		elif Input.is_action_just_released("ui_up") and $chat_saut.get_frame() == 0:
			s = 0

			
#		#couche
		elif Input.is_action_just_pressed("ui_down") and q == 0:
			if last == "right":
				$chat.flip_h = false
			if last == "left":
				$chat.flip_h = true
			$chat.play("transition3")
			motion.x = 0
		elif Input.is_action_pressed("ui_down") and $chat.get_frame() == 1:
			if last == "right":
				$chat.flip_h = true
			if last == "left":
				$chat.flip_h = false
			q = 1
			motion.x = 0
			$chat.play("couche")
		elif Input.is_action_just_released("ui_down") and q == 0:
			if last == "right":
				$chat.flip_h = false
			if last == "left":
				$chat.flip_h = true
			$chat.play("transition3")
			motion.x = 0
		
		#assis
		
		else:
			if $chat.get_frame() == 1 and q == 0:
				$chat.play("idle")
				motion.x = 0
			
			
	motion = move_and_slide(motion, UP)
	
	pass
