extends Sprite

var imagetexture = ImageTexture.new()
var decalage = 4
var dec
var att = 0
var spd = 6
var i = 0
var x
var y
var compt = 0



func _process(delta):
	compt = compt + 1 
	att = att + 1
	if compt > 2 :
		var img = Image.new()
		img.create(180,30,false,Image.FORMAT_RGBA8)
		var img2 = get_texture().get_data()
		img2.lock()
		img.lock()
		if att > spd :
			i = (i + 1)%(2 * decalage)
			att = 0
		for y in range (0,20) :
			if abs(y-i)/decalage % 2 == 0 :
				dec = abs(y-i)%decalage
			else :
				dec = decalage - abs(y-i)%decalage
			for x in range (10,170) :
				img.set_pixel(x-dec,y,img2.get_pixel(x,y))
		imagetexture.create_from_image(img)
		set_texture(imagetexture)
		update()
		
	
	