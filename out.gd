extends Area2D

export(String, FILE, "*.tscn") var next_place

func _process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "player" or body.name == "mala":
			get_tree().change_scene(next_place)
	pass


func _on_out_body_entered(body):
	pass # replace with function body
