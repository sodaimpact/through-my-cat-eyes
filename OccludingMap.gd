extends Sprite

var finalTexture = ImageTexture.new()
var compt = 0
var bleu
var LSpoints = []
var LSdroites = []
var pix
var floprec = 0.01



var sun1 = Vector2(60.5,150) #Position de la source de lumière 1
var sun2 = Vector2(80.5,150) #Position de la source de lumière 2
var c = 0.03 #Change le coeficient par lequel la transparance décroît.
var a = 0    #Pente de la droite pixel-source de lumière.
var alpha1 = 0
var alpha2 = 0
var maxi = 0
var limit = [100,100,100]
var cycle = 0
var x1 = 0
var x2 = 0
var x11
var x22
var temp = 0




func _process(delta):
	if compt >=10 and temp == 0:
		var OcMap = OcclusionMap()
		OcMap.lock()
		for pixel in LSpoints :
			a = stepify((pixel.y - sun1.y) / (pixel.x - sun1.x),floprec)
			LSdroites.append(Vector2(a, stepify((pixel.y - a*pixel.x),floprec)))
			a = stepify((pixel.y - sun2.y) / (pixel.x - sun2.x),floprec)
			LSdroites.append(Vector2(a, stepify((pixel.y - a*pixel.x) , floprec )))
			maxi = max(maxi,pixel.y)
#		print(len(LSdroites),len(LSpoints))
		for x in range (0,160,2) :
			for y in range(limit[cycle],maxi,2) :
				pix = OcMap.get_pixel(x,y)
				if pix == bleu or pix == Color(1,0,0,1) :
					OcMap.set_pixel(x,y,bleu)
				else:
					OcMap.set_pixel(x,y,Color(0,0,1,0))
				for i in range(0,len(LSpoints)) :
					if LSpoints[i].x < sun1.x :
						if y > LSdroites[2*i].x*x+LSdroites[2*i].y and y < LSdroites[2*i+1].x*x+LSdroites[2*i+1].y :
							OcMap.set_pixel(x,y,bleu)
							OcMap.set_pixel(x+1,y,bleu)
							OcMap.set_pixel(x+1,y+1,bleu)
							OcMap.set_pixel(x,y+1,bleu)
#							alpha1 = 1-(144-y)*c
#							alpha2 = 1-(144-y-1)*c
#							OcMap.set_pixel(x,y,Color(bleu.r,bleu.g,bleu.b,max(0,alpha1)))
#							OcMap.set_pixel(x+1,y,Color(bleu.r,bleu.g,bleu.b,max(0,alpha1)))
#							OcMap.set_pixel(x+1,y+1,Color(bleu.r,bleu.g,bleu.b,max(0,alpha2)))
#							OcMap.set_pixel(x,y+1,Color(bleu.r,bleu.g,bleu.b,max(0,alpha2)))
					elif LSpoints[i].x > sun2.x :
						if y < LSdroites[2*i].x*x+LSdroites[2*i].y and y > LSdroites[2*i+1].x*x+LSdroites[2*i+1].y :
							OcMap.set_pixel(x,y,bleu)
							OcMap.set_pixel(x+1,y,bleu)
							OcMap.set_pixel(x+1,y+1,bleu)
							OcMap.set_pixel(x,y+1,bleu)
#							alpha1 = 1-(144-y)*c
#							alpha2 = 1-(144-y-1)*c
#							OcMap.set_pixel(x,y,Color(bleu.r,bleu.g,bleu.b,max(0,alpha1)))
#							OcMap.set_pixel(x+1,y,Color(bleu.r,bleu.g,bleu.b,max(0,alpha1)))
#							OcMap.set_pixel(x+1,y+1,Color(bleu.r,bleu.g,bleu.b,max(0,alpha2)))
#							OcMap.set_pixel(x,y+1,Color(bleu.r,bleu.g,bleu.b,max(0,alpha2)))
					else :
						if y < LSdroites[2*i].x*x+LSdroites[2*i].y and y < LSdroites[2*i+1].x*x+LSdroites[2*i+1].y :
							OcMap.set_pixel(x,y,bleu)
							OcMap.set_pixel(x+1,y,bleu)
							OcMap.set_pixel(x+1,y+1,bleu)
							OcMap.set_pixel(x,y+1,bleu)
#							alpha1 = 1-(144-y)*c
#							alpha2 = 1-(144-y-1)*c
#							OcMap.set_pixel(x,y,Color(bleu.r,bleu.g,bleu.b,max(0,alpha1)))
#							OcMap.set_pixel(x+1,y,Color(bleu.r,bleu.g,bleu.b,max(0,alpha1)))
#							OcMap.set_pixel(x+1,y+1,Color(bleu.r,bleu.g,bleu.b,max(0,alpha2)))
#							OcMap.set_pixel(x,y+1,Color(bleu.r,bleu.g,bleu.b,max(0,alpha2)))
#				pix = OcMap.get_pixel(x,y)
#				if pix == bleu or pix == Color(1,0,0,1) :
#					OcMap.set_pixel(x,y,bleu)
#				else:
#					OcMap.set_pixel(x,y,Color(0,0,1,0))
#				for i in range(0,len(LSpoints)) :
#					if LSpoints[i].x < sun1.x :
#						if y > LSdroites[2*i].x*x+LSdroites[2*i].y and y < LSdroites[2*i+1].x*x+LSdroites[2*i+1].y :
#							OcMap.set_pixel(x,y,bleu)
##							OcMap.set_pixel(x,y,Color(bleu.r,bleu.g,bleu.b,max(0,1-(144-y)*c)))
#					elif LSpoints[i].x > sun2.x :
#						if y < LSdroites[2*i].x*x+LSdroites[2*i].y and y > LSdroites[2*i+1].x*x+LSdroites[2*i+1].y :
#							OcMap.set_pixel(x,y,bleu)
##							OcMap.set_pixel(x,y,Color(bleu.r,bleu.g,bleu.b,max(0,1-(144-y)*c)))
#					else :
#						if y < LSdroites[2*i].x*x+LSdroites[2*i].y and y < LSdroites[2*i+1].x*x+LSdroites[2*i+1].y :
#							OcMap.set_pixel(x,y,bleu)
##							OcMap.set_pixel(x,y,Color(bleu.r,bleu.g,bleu.b,max(0,1-(144-y)*c)))
#			for y in range(80,84,4) :
#				pix = OcMap.get_pixel(x,y)
#				if pix == bleu or pix == Color(1,0,0,1) :
#					OcMap.set_pixel(x,y,bleu)
#				else:
#					OcMap.set_pixel(x,y,Color(0,0,1,0))
#				for i in range(0,len(LSpoints)) :
#					if LSpoints[i].x < sun1.x :
#						if y > LSdroites[2*i].x*x+LSdroites[2*i].y and y < LSdroites[2*i+1].x*x+LSdroites[2*i+1].y :
#							OcMap.set_pixel(x,y,bleu)
##							OcMap.set_pixel(x,y,Color(bleu.r,bleu.g,bleu.b,max(0,1-(144-y)*c)))
#					elif LSpoints[i].x > sun2.x :
#						if y < LSdroites[2*i].x*x+LSdroites[2*i].y and y > LSdroites[2*i+1].x*x+LSdroites[2*i+1].y :
#							OcMap.set_pixel(x,y,bleu)
##							OcMap.set_pixel(x,y,Color(bleu.r,bleu.g,bleu.b,max(0,1-(144-y)*c)))
#					else :
#						if y < LSdroites[2*i].x*x+LSdroites[2*i].y and y < LSdroites[2*i+1].x*x+LSdroites[2*i+1].y :
#							OcMap.set_pixel(x,y,bleu)
##							OcMap.set_pixel(x,y,Color(bleu.r,bleu.g,bleu.b,max(0,1-(144-y)*c)))
#		for y in range (75,144) :
#			for i in range(0,len(LSpoints)):
#				if y <= LSpoints[i].y :
#					x1 = floor((y - LSdroites[2*i].y)/LSdroites[2*i].x)
#					x2 = floor((y - LSdroites[2*i+1].y)/LSdroites[2*i+1].x)
#					x11 = min(x1,x2)
#					x22 = max(x1,x2)
##				print("xi",x11,"x2",x22)
#					OcMap.set_pixel(x11,y,bleu)
#					OcMap.set_pixel(x22,y,bleu)
#				for x in range (x11,x22) :
#					print(x)
#					OcMap.set_pixel(x,y,bleu)

		finalTexture.create_from_image(OcMap)
		set_texture(finalTexture)
		update()
		self.show()
		OcMap.unlock()
		temp=(temp+1)%2
		cycle= (cycle + 1)%3
	else : 
		self.hide()
		temp=(temp+1)%2
		compt = compt + 1
#	pass
func OcclusionMap() :
	LSdroites = []
	LSpoints = []
	var img = Image.new()
	img.create(160,144,false,Image.FORMAT_RGBA8)
	var textr = $refscene.get_texture()
	var img2 = textr.get_data()
	img2.lock()
	img.lock()
	if compt == 10 :
		self.show()
		bleu = img2.get_pixel(10,140)
		compt=compt+1
	for y in range (110,144) :
		for x in range (0,160) :
			if img2.get_pixel(x,y) == Color(1,0,0,1) :
				img.set_pixel(x,y,bleu)
				LSpoints.append(Vector2(x,y))
#			else :
#				img.set_pixel(x,y,Color(0,0,0,0))
	img2.unlock()
	img.unlock()
	return img
	