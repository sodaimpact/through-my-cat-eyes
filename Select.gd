extends Node2D

var pos = 0

export(String, FILE, "*.tscn") var next_place

func _process(delta):
	if Input.is_action_just_pressed('ui_up') or Input.is_action_just_pressed('ui_down'):
		pos = pos +1
		pos = pos % 2
		$play.play("stop")
		$option.play("stop")
		$selectsound.play()
	if pos == 0 :
		$play.play("select")
		$option.play("stop")
	if pos == 1 :
		$play.play("stop")
		$option.play("select")
	if Input.is_action_pressed('ui_accept') and pos == 0:
		get_tree().change_scene(next_place)
		
	pass