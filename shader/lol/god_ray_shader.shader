shader_type canvas_item;

uniform vec2 light_position ;
uniform int samples ;
uniform float weight ;
uniform float decay ;

uniform vec4 color0 : hint_color;
uniform vec4 color1 : hint_color;
uniform vec4 color2 : hint_color;

float delta_squared(float a, float b) {
	return (a - b)*(a - b);
}

float color_distance_squared(vec4 a, vec4 b) {
	return 2.0 * delta_squared(a.r, b.r) + 4.0 * delta_squared(a.g, b.g) + 3.0 * delta_squared(a.b, b.b);
}

float luminance(vec4 color) {
	return (color.r + color.g + color.b) / 3.0;
}

vec4 grayscale(vec4 color) {
	return vec4(vec3(1.0) * luminance(color), color.a);
}

vec4 contrast(vec4 color, float factor, float pivot) {
	return vec4((color.rgb - vec3(pivot)) * factor + vec3(pivot), color.a);
}

vec4 light_pass(vec4 color) {
	return vec4(vec3(1.0), float(luminance(color) > 0.5));
}

vec4 remove_color(vec4 base_color, vec4 color_to_remove) {
	if (color_distance_squared(base_color, color_to_remove) < 0.1) {
		return vec4(0.0);
	} else {
		return base_color;
	}
}

void fragment() {
	// GOD'S RAYS
	
	COLOR = texture(TEXTURE, UV);
	float ilumination = weight;
	vec2 uv = UV;
	vec2 delta = (light_position - UV) / float(samples);
	
	vec4 buffer = vec4(0.0);
	
//	COLOR = light_pass(contrast(grayscale(COLOR), 10.0, 0.1));
//	COLOR = remove_color(COLOR, color0);
	
	for (int i = 0; i < samples; i++) {
		uv += delta;
		vec4 sample = texture(TEXTURE, uv);
		buffer += contrast(grayscale(remove_color(sample, color0)), 1.0, 0.1) * ilumination;
		ilumination *= decay;
	}
	
	if (luminance(buffer) > 0.1) COLOR = color0;
//	COLOR = buffer;

//	// COLOR GRADING
//
//	float color0_distance = color_distance_squared(COLOR, color0);
//	float color1_distance = color_distance_squared(COLOR, color1);
//	float color2_distance = color_distance_squared(COLOR, color2);
//
//	float min_distance = min(color0_distance, min(color1_distance, color2_distance));
////	float min_distance = min(color1_distance, color2_distance);
//
//	if (min_distance == color0_distance) COLOR = color0;
//	else if (min_distance == color1_distance) COLOR = color1;
//	else COLOR = color2;

// LOL
}