extends Sprite

var light_point;
var show_shader = true
var shader

func _ready():
	light_point = get_node("/root/forêt/player/Camera2D")
	shader = material

func _process(delta):
	if show_shader == true : 
		material = shader 
		var x = light_point.get_camera_position().x / texture.get_width()
		material.set_shader_param("light_position", Vector2(x, 0))
	else: 
		material = null
		
	show_shader = not show_shader