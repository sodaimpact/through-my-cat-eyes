extends KinematicBody2D


func _ready():
	$sak/APsak.queue("sans")
	$back_parc_avec.set_visible(false)
	pass
	
	
func _process(delta):
	
	if Input.is_action_pressed("ui_sak_sans"):
		$sak/APsak.stop(true)
		$back_parc_avec.set_visible(false)
		$sak/APsak.queue("sans")
		
	if Input.is_action_pressed("ui_sak_avec"):
		$sak/APsak.stop(true)
		$back_parc_avec.set_visible(true)
		$sak/APsak.queue("avec")
		
	pass