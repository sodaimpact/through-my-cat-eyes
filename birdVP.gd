extends Node2D

var n = 0
var dir

var x = 0
var y = 0
var dx = rand_range(0.1,0.5)
var dy = rand_range(0.1,0.5)
var a = rand_range(0.0002,0.001)
var b = rand_range(0.0002,0.001)
var go = 0



func _ready():
	$bird_f.set_position(Vector2(x,y))
	dir = divers.last
	
	pass


func _process(delta):
	
	$bird_f/APbird_f.queue("fly")

	if get_parent().get_position().x > 25 and $player.get_position().x < 115 or go == 1:
		go = 1
		n+=1
		y -= dy
		dy += n*a
		if dir == "R" :
			$bird_f.flip_h = false
			x += dx
			dx += n*b
		if dir == "L" :
			$bird_f.flip_h = true
			x -= dx
			dx += n*b

		$bird_f.set_position(Vector2(x,y))
	
	
	
	
	pass
