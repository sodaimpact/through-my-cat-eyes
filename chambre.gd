extends Node2D

var piste = 12
var joue 
var scene
var X_chat
var Y_chat
var X_mala
var Y_mala
var a_chat = 10
var a_mala = 6

func _ready():
	$player_m.set_visible(false)
	$mala_m.set_visible(false)
	$masque.set_visible(true)
	
	#musique
	joue = musiques.encours()
	if joue != piste :
		musiques.jouer(piste)
	
	#spawn
	scene = spawn.scene
	if scene == -1 :
		$player.set_position(Vector2(284,97))
	elif scene == 2 :
		$player.set_position(Vector2(284,97))
	else :
		$player.set_position(Vector2(0,0))
	spawn.scene = 0
	
	pass

func _process(delta):
	X_chat = $player.get_position().x
	Y_chat = $player.get_position().y
	X_mala = $mala.get_position().x
	Y_mala = $mala.get_position().y
	
	if Y_chat <= 76 and X_chat <= 100 and Input.is_action_pressed("ui_up") == false :
		piste = 13
		$background/screen.show()
		joue = musiques.encours()
		if joue != piste :
			musiques.jouer(piste)
			
	if X_chat >= 125 and X_chat <= 180 :
		$player_m.set_visible(true)
		$player_m.set_position(Vector2(X_chat + a_chat,Y_chat))
		
	else:
		$player_m.set_visible(false)
		
	if X_mala >= 120 and X_mala <= 175 :
		$mala_m.set_visible(true)
		$mala_m.set_position(Vector2(X_mala + a_mala,Y_mala))
		
	else:
		$mala_m.set_visible(false)
		
		
			
	pass
			
			