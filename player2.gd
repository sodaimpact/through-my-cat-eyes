extends KinematicBody2D

#################

var motion = Vector2()
const UP = Vector2(0,-1)
var gravity = 10
var maxspeed = 50	#50
var acc = 5				#acceleration
var last
var saut = 0			#lorsque le joueur saute
var couche = 0			#lorsque le joueur est couche
var fall = 0			#lorsque le joueur tombe
var coup = 0

#################

func _ready():
	$CSdefaut.set_disabled(true)
	$CSfrontR.set_disabled(false)
	$CSfrontL.set_disabled(false)
	$CScouche.set_disabled(false)
	#$CSidleR.set_disabled(false)
	$AcoupR.set_monitorable(false)
	$AcoupL.set_monitorable(false)
	pass
	
#################

func _physics_process(delta):
	
	motion.y += gravity
	
	
	#anti bug
	#proleme gauche droite ensemble
	#saut multiple entre transition village chambre
	
	
	#DROITE
	if Input.is_action_just_pressed("ui_right") and is_on_floor():		#se releve
		$CSidleR.set_disabled(true)		#effectue modification collision shape
		$CSidleL.set_disabled(true)
		$CSmarcheL.set_disabled(true)
		$CStransL.set_disabled(true)
		$CSup.set_disabled(true)
		$CSfrontL.set_disabled(true)
		$CScouche.set_disabled(true)
		$CSfrontR.set_disabled(false)
		$CStransR.set_disabled(false)
		
		couche = 0		#annule action precedente
		divers.last = "R"		#enregistre direction
		$chat.flip_h = false		#direction sprite
		$chat/APidle.stop(true)		#arrete animation en cours
		$chat/APcouche.stop(true)
		$chat/APtrans.play("transition",1,2)		#lance animation 
		motion.x = 0		#mouvement
			
	elif Input.is_action_pressed("ui_right") and is_on_floor() and not(Input.is_action_pressed("ui_left")):		#marche
		$chat.flip_h = false		#direction sprite
	
		if not($chat/APtrans.is_playing()):		#lorsque la transition est finie

			$CStransR.set_disabled(true)		#effectue modification collision shape
			$CSmarcheR.set_disabled(false)
			$CSfrontR.set_disabled(false)		#evite les bugs collisions lors des changement de panneaux
			
			$chat/APtrans.stop(true)		#arrete animation en cours
			$chat/APmarche.queue("marche")		#lance animation 
			motion.x = min(motion.x + acc, maxspeed)		#mouvement accelere borné par max speed
			
	elif Input.is_action_just_released("ui_right") and is_on_floor():		#se rassoit
		$CSmarcheR.set_disabled(true)		#effectue modification collision shape
		$CStransR.set_disabled(false)
		$chat.flip_h = false		#direction sprite
		$chat/APmarche.stop(true)		#arrete animation en cours
		$chat/APtrans.play("transition")		#lance animation 
		motion.x = 0		#mouvement
		
		
	#GAUCHE (meme annotation que precedemment)
	elif Input.is_action_just_pressed("ui_left") and is_on_floor():
		$CSidleR.set_disabled(true)
		$CSidleL.set_disabled(true)
		$CSmarcheR.set_disabled(true)
		$CStransR.set_disabled(true)
		$CSup.set_disabled(true)
		$CSfrontR.set_disabled(true)
		$CScouche.set_disabled(true)
		$CSfrontL.set_disabled(false)
		$CStransL.set_disabled(false)
		
		couche = 0
		#last = "L"
		divers.last = "L"
		$chat.flip_h = true
		$chat/APidle.stop(true)
		$chat/APcouche.stop(true)
		$chat/APtrans.play("transition",1,2)
		motion.x = 0
		
	elif Input.is_action_pressed("ui_left") and is_on_floor() and not(Input.is_action_pressed("ui_right")):
		$chat.flip_h = true
		
		if not($chat/APtrans.is_playing()):

			$CStransL.set_disabled(true)
			$CSmarcheL.set_disabled(false)
			$CSfrontL.set_disabled(false)

			
			$chat/APtrans.stop(true)
			$chat/APmarche.queue("marche")
			motion.x = max(motion.x - acc, -maxspeed)
			
	elif Input.is_action_just_released("ui_left") and is_on_floor():
		$CSmarcheL.set_disabled(true)
		$CStransL.set_disabled(false)
		$chat.flip_h = true
		$chat/APmarche.stop(true)
		$chat/APtrans.play("transition")
		motion.x = 0
	
	
	#SAUT
	elif Input.is_action_just_pressed("ui_up") and saut == 0 and is_on_floor():		#se prepare a saute si il n a pas deja saute
		$CSmarcheR.set_disabled(true)		#effectue modification collision shape
		$CSmarcheL.set_disabled(true)
		$CSfrontL.set_disabled(true)
		$CSfrontR.set_disabled(true)
		$CSidleR.set_disabled(true)
		$CSidleL.set_disabled(true)
		$CScouche.set_disabled(true)
		
		couche = 0		#annule action precedente
		$chat/APidle.stop(true)		#arrete animation en cours
		$chat/APcouche.stop(true)
		$chat/APtrans.stop(true)
		$chat/APmarche.stop(true)
		
		if divers.last == "R":		#direction sprite en fonction de la derniere position
			$chat.flip_h = false
			$CStransR.set_disabled(false)		#effectue modification collision shape
		else:
			$chat.flip_h = true
			$CStransL.set_disabled(false)		#effectue modification collision shape
			
		$chat/APtrans.play("transition")		#lance animation
		motion.x = 0		#mouvement
		
	elif Input.is_action_pressed("ui_up") and is_on_floor():		#lance le saut
		
		if not($chat/APtrans.is_playing()) and saut == 0:		#lorsque la transition est finie
			$CSup.set_disabled(false)		#effectue modification collision shape
			$chat/APtrans.stop(true)		#arrete animation en cours
			saut = 1		#enregistre que le chat a deja saute une fois
			$chat/APsaut.play("saut")		#lance animation
			motion.y = -170		#mouvement
			
		elif not($chat/APsaut.is_playing()) and saut == 1:		#si le saut est finie et qu'il vient de sauter et que le joueur garde le bouton up appuye
			$CSup.set_disabled(true)		#effectue modification collision shape
			$chat/APtrans.play("transition")		#lance animation
			
	elif saut == 1 and is_on_floor():		#si le saut est realise et que le joueur a lache le bouton up
		$CSup.set_disabled(true)		#effectue modification collision shape
		saut = 0		#le joueur peut de nouveau sauter
		$chat/APsaut.stop(true)		#arrete animation en cours
		$chat/APtrans.play("transition")		#lance animation
		
		
	#TOMBE
	elif not(is_on_floor()) and saut == 0:		#si le joueur est en l air et qu il ne saute pas donc tombe
		$CSmarcheR.set_disabled(true)		#effectue modification collision shape
		$CSmarcheL.set_disabled(true)
		$CSup.set_disabled(true)
		$CSfrontL.set_disabled(true)
		$CSfrontR.set_disabled(true)
		$CSidleR.set_disabled(true)
		$CSidleL.set_disabled(true)
		$CScouche.set_disabled(true)
		$CStransR.set_disabled(true)
		$CStransL.set_disabled(true)
		
		if divers.last == "R":		#direction sprite en fonction de la derniere position
			$chat.flip_h = false
			$CStransR.set_disabled(false)		#effectue modification collision shape
		else:
			$chat.flip_h = true
			$CStransL.set_disabled(false)		#effectue modification collision shape
		
		$chat/APtrans.stop(true)		#arrete animation en cours
		$chat/APmarche.stop(true)
		$chat/APidle.stop(true)
		$chat/APsaut.play("down")		#lance animation
		fall = 1		#le joueur tombe
		
	elif fall == 1 and is_on_floor():		#si le joueur tombe et qu il vient de retourner au sol
		fall = 0		#il peut de nouveau tomber
		$chat/APsaut.stop(true)		#arrete animation en cours
		$chat/APtrans.play("transition",1,0.7)		#lance animation
		motion.x = 0		#on bloque le mouvement pour eviter de glisser
		
		
	#COUCHE
	elif Input.is_action_just_pressed("ui_down") and is_on_floor() and couche == 0:		#transition pour se coucher si le chat est pas deja couche
		$CSmarcheR.set_disabled(true)		#effectue modification collision shape
		$CSmarcheL.set_disabled(true)
		$CSup.set_disabled(true)
		$CSfrontL.set_disabled(true)
		$CSfrontR.set_disabled(true)
		$CSidleR.set_disabled(true)
		$CSidleL.set_disabled(true)
		
		if divers.last == "R":		#direction sprite en fonction de la derniere position
			$chat.flip_h = false
			$CStransR.set_disabled(false)		#effectue modification collision shape
		else:
			$chat.flip_h = true
			$CStransL.set_disabled(false)		#effectue modification collision shape
			
		$chat/APidle.stop(true)		#arrete animation en cours
		$chat/APmarche.stop(true)
		$chat/APtrans.play("transition",1,0.7)		#lance animation
		motion.x = 0		#on bloque le mouvement pour pas glisser
		
	elif Input.is_action_pressed("ui_down") and is_on_floor():
		
		if divers.last == "R":		#direction sprite en fonction de la derniere position
			$chat.flip_h = false
		else:
			$chat.flip_h = true
		
		if not($chat/APtrans.is_playing()):		#si la transition est finie
			$CStransR.set_disabled(true)		#effectue modification collision shape
			$CStransL.set_disabled(true)
			$CScouche.set_disabled(false)
			couche = 1		#le chat est couche on veut bloquer cette action
			$chat/APtrans.stop(true)		#arrete animation en cours
			$chat/APcouche.queue("couche")		#lance animation
			motion.x = 0		#on bloque le mouvement au cas ou
			
	elif is_on_floor() and couche == 1:		#lorsque le joueur s est couche pour le bloquer au sol
		if not($chat/APtrans.is_playing()):
			$chat/APtrans.stop(true)		#arrete animation en cours
			$chat/APcouche.queue("couche")		#lance animation
			motion.x = 0		#on bloque le mouvement au cas ou
	
	
	#COUP
	elif Input.is_action_just_pressed("ui_coup") and is_on_floor() and coup == 0 :
		$CSup.set_disabled(true)		#effectue modification collision shape
		$CSfrontL.set_disabled(true)
		$CSfrontR.set_disabled(true)
		$CSidleR.set_disabled(true)
		$CSidleL.set_disabled(true)
		$CSmarcheR.set_disabled(true)
		$CSmarcheL.set_disabled(true)
		
		$chat/APtrans.stop(true)		#arrete animation en cours
		$chat/APmarche.stop(true)
		$chat/APidle.stop(true)
		$chat/APcouche.stop(true)
		
		if divers.last == "R":
			$CStransR.set_disabled(false)
			$chat.flip_h = false
		else :
			$CStransL.set_disabled(false)
			$chat.flip_h = true
		
		$chat/APtrans.play("transition",1,2)
		motion.x = 0
		
	elif Input.is_action_pressed("ui_coup") and is_on_floor() and coup == 0 and not($chat/APtrans.is_playing()):
		$chat/APtrans.stop(true)
		
		if divers.last == "R":
			$CSmarcheR.set_disabled(false)
			$AcoupR/CScoupR.set_disabled(false)
			$AcoupR.set_monitorable(true)
			$chat.flip_h = false
		else :
			$CSmarcheL.set_disabled(false)
			$AcoupL/CScoupL.set_disabled(false)
			$AcoupL.set_monitorable(true)
			$chat.flip_h = true
		
		coup = 1
		$chat/APcoup.queue("coup")
		motion.x = 0
		
	elif not($chat/APcoup.is_playing()) and coup == 1 and Input.is_action_pressed("ui_coup"):
		coup = 0
		
	elif not($chat/APcoup.is_playing()) and coup == 1 :
		$CSmarcheR.set_disabled(true)
		$CSmarcheL.set_disabled(true)
		$AcoupR/CScoupR.set_disabled(true)
		$AcoupL/CScoupL.set_disabled(true)
		$AcoupR.set_monitorable(false)
		$AcoupL.set_monitorable(false)
		$chat/APcoup.stop(true)
		
		if divers.last == "R":
			$CStransR.set_disabled(false)
			$chat.flip_h = false
		else :
			$CStransL.set_disabled(false)
			$chat.flip_h = true
		
		$chat/APtrans.play("transition")
		coup = 0
		motion.x = 0
	
	
	#ASSIS
	elif not($chat/APtrans.is_playing()) and is_on_floor() and not($chat/APcoup.is_playing()):		#si la transition est finie
		$CStransR.set_disabled(true)		#effectue modification collision shape
		$CStransL.set_disabled(true)
		$CSmarcheR.set_disabled(true)
		$CSmarcheL.set_disabled(true)
		$CSup.set_disabled(true)
		$CScouche.set_disabled(true)
		
		if divers.last == "R":		#direction sprite en fonction de la derniere position
			$chat.flip_h = false
			$CSidleR.set_disabled(false)		#effectue modification collision shape
		else:
			$chat.flip_h = true
			$CSidleL.set_disabled(false)		#effectue modification collision shape
			
		$chat/APidle.queue("idle")		#lance animation
		motion.x = 0		#on bloque le mouvement
		
		
	motion = move_and_slide(motion, UP)		#application du mouvement
	
	pass
