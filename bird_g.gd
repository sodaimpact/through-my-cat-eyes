extends Node2D

onready var player = get_node("/root/forêt/player")

var hit = divers.hit

func _ready():
	if hit == 0 :
		$bird_i.set_visible(true)
		$bird_f.set_visible(false)
		$bird_f2.set_visible(false)
		$bird_i.set_position(Vector2(87,81))
		$bird_f.set_position(Vector2(87,81))
		$bird_f2.set_position(Vector2(90,81))
	
	pass

func _process(delta):
	if hit == 0 :
		if player.get_position().x > 25 and player.get_position().x < 115:
			$bird_i.set_visible(false)
			$bird_f.set_visible(true)
			$bird_f2.set_visible(true)
			
		else:
			$bird_i.play("idle")
	
	pass