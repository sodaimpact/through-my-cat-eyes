extends Sprite

var finalTexture = ImageTexture.new()
var compt = 0
var bleu
var bleu2


var show_shader = true
var shader


func _ready():
	shader = material



func _process(delta):
	
	if compt >=100 :
#		var OcMap = OcclusionMap()
#		OcMap.lock()
#		finalTexture.create_from_image(OcMap)
#		self.set_texture(finalTexture)
#		update()
		self.show()
#		OcMap.unlock()
		pass
	else : 
		self.hide()
		compt = compt + 1
		
	if show_shader == true : 
		material = shader 
		
	else: 
		material = null
		
	show_shader = not show_shader

func OcclusionMap() :

	var img = Image.new()
	img.create(160,144,false,Image.FORMAT_RGBA8)
	var textr = $refscene.get_texture()
	var img2 = textr.get_data()
	img2.lock()
	img.lock()
	if compt == 100 :
		self.show()
		bleu = img2.get_pixel(10,140)
		bleu2 = img2.get_pixel(10,0)
		compt=compt+1
	for y in range (100,144) :
		for x in range (0,160) :
			if img2.get_pixel(x,y) == bleu :
				img.set_pixel(x,y,bleu)
			else : 
				img.set_pixel(x,y,bleu2)
				
	for y in range(0,100) :
		for x in range(0,160) :
			img.set_pixel(x,y,bleu2)

	img2.unlock()
	img.unlock()
	return img
	
